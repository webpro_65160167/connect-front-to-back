import type { Member } from "@/types/Member";
import http from "./http";

function addMember(member: Member) {
    return http.post('/members', member)
}

function updateMember(member: Member) {
    return http.patch(`/members/${member.id}`, member)
}

function delMember(member: Member) {
    return http.delete(`/members/${member.id}`)
}

function getMember(id: number) {
    return http.get(`/members/${id}`)
}

function getAllMember() {
    return http.get('/members')
}

export default {addMember, updateMember, delMember, getMember, getAllMember}