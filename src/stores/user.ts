import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import userService from '@/services/user'
import type { User } from '@/types/User'

export const useUserStore = defineStore('user', () => {
    const users = ref<User[]>([])
    const loadingStore = useLoadingStore()
    async function getUser() {
        loadingStore.doLoad()
        const res = await userService.getAllUser()
        users.value = res.data
        loadingStore.finish()
      }
      async function getUserById(id: number) {
        loadingStore.doLoad()
        const res = await userService.getUser(id)
        users.value = res.data
        loadingStore.finish()
      }
      
      async function saveUser(user: User) {
        loadingStore.doLoad()
        if (user.id < 0) {
          // Add new
          const res = await userService.addUser(user)
        } else {
          // Update
          const res = await userService.updateUser(user)
        }
        await getUser()
        loadingStore.finish()
      }
      
      async function deleteUser(user: User) {
        loadingStore.doLoad()
        const res = await userService.delUser(user)
        await getUser()
        loadingStore.finish()
      }
  return { users, getUser, saveUser, deleteUser, getUserById }
})
