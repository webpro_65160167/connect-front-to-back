import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'
import { useLoadingStore } from './loading'
import memberService from '@/services/member'

export const useMemberStore = defineStore('member', () => {
  const getMemberByTel = (tel: string): Member | null => {
    for (const member of members.value) {
      if (member.tel === tel) return member
    }
    return null
  }
  const members = ref<Member[]>([])
    const loadingStore = useLoadingStore()
    async function getMember() {
        loadingStore.doLoad()
        const res = await memberService.getAllMember()
        members.value = res.data
        loadingStore.finish()
      }
      async function getMemberById(id: number) {
        loadingStore.doLoad()
        const res = await memberService.getMember(id)
        members.value = res.data
        loadingStore.finish()
      }
      
      async function saveMember(member: Member) {
        loadingStore.doLoad()
        if (member.id < 0) {
          // Add new
          const res = await memberService.addMember(member)
        } else {
          // Update
          const res = await memberService.updateMember(member)
        }
        await getMember()
        loadingStore.finish()
      }
      
      async function deleteMember(member: Member) {
        loadingStore.doLoad()
        const res = await memberService.delMember(member)
        await getMember()
        loadingStore.finish()
      }
  return { members, getMember, saveMember, deleteMember, getMemberById }
})
